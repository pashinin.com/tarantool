FROM tarantool/tarantool:2.6.3
RUN apk add --no-cache --virtual .build-deps \
    gcc \
    musl-dev \
    wget \
    unzip \
    && tarantoolctl rocks install lua-cjson --server=https://luarocks.org \
    && apk del .build-deps
COPY src /opt/tarantool
# RUN [ "tarantoolctl", "rocks", "install", "gis" ]
# RUN [ "tarantoolctl", "rocks", "install", "expirationd2" ]
CMD [ "tarantool", "/opt/tarantool/app.lua" ]
