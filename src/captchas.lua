-- Create 'captchas' space
box.once(
   "captchas_space",
   function()
      box.schema.space.create('captchas',{id=1, if_not_exists=true})
      box.space.captchas:create_index('primary', {type = 'hash', unique=true, parts = {1, 'string'}})
      -- box.space.captchas:create_index('expired_at', {type = 'hash', unique=true, parts = {3, 'time'}})
   end
)

fiber = require('fiber')
expd = require('expirationd')
function is_tuple_expired(args, tuple)
   if (tuple[3] == nil) then return true end
   if (tuple[3] < fiber.time()) then return true end
   return false
end
function delete_tuple(space_id, args, tuple)
   box.space[space_id]:delete{tuple[1]}
end
expd.start(
   'expire_captchas',
   box.space.captchas.id,
   is_tuple_expired,
   {
      process_expired_tuple = delete_tuple,
      args = nil,
      tuples_per_iteration = 50,
      full_scan_time = 3600,
      force = true,
   }
)
