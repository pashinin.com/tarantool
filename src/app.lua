-- "box.cfg" is needed to start Tarantool event loop
box.cfg {
    listen = 3301,
    -- work_dir = "tmp",
}

-- box.once(
--     "bootstrap",
--     function()
--         box.schema.user.create('user', { password = 'secret' })
--         box.schema.user.grant('user', 'read,write,execute', 'universe')
--     end
-- )


captchas = require('captchas')
mails = require('mails')
