#!/bin/bash

# $1 - branch name

mkdir -p tmp
VERSION_TAG=`git rev-parse --short HEAD`
BRANCH=$1
JOB=nomad-$BRANCH.job.tpl
if [ ! -f "$JOB" ]; then  # for tags - use stable version
    JOB=nomad-stable.job.tpl
fi
DATE=`date`
# cat nomad-$1.job | docker run -i --rm -e STABLE=stable -e LATEST=master subfuzion/envtpl
export DATE=$DATE && export VERSION=$VERSION_TAG && export BRANCH=$BRANCH && envsubst < "$JOB" > "tmp/nomad-$BRANCH.job"
nomad run tmp/nomad-$BRANCH.job
