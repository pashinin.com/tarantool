# -*- mode: hcl -*-
job "tarantool" {
  datacenters = ["moscow"]
  type = "service"

  constraint {
    attribute = "${meta.instance_type}"
    value = "worker"
  }
  constraint {
    distinct_hosts = true
  }

  # Specify this job to have rolling updates, one-at-a-time, with
  # 30 second intervals.
  update {
    stagger      = "30s"
    max_parallel = 1
  }

  group "tarantool-master-1" {
    count = 1  # one group of 3 masters
    constraint {
      attribute = "${attr.unique.hostname}"
      value     = "node1"
    }

    restart {
      # mode = "fail"  # fail, delay (default)
      interval = "1m"
      attempts = 1
      delay = "30s"  # minimum wait
    }

    task "tnt-master-1" {
      driver = "docker"
      config {
        image = "registry.gitlab.com/pashinin.com/tarantool:${TAG}"
        force_pull = true
        network_mode = "host"

        port_map {
          tarantool = 3301  # port used inside a container
        }

        volumes = [
          "/var/lib/tarantool:/var/lib/tarantool",
        ]
      }

      env {
        TARANTOOL_REPLICATION_SOURCE = "10.254.239.3,10.254.239.5"
        # TARANTOOL_REPLICATION_SOURCE = "10.254.239.5"
        VERSION = "master-${VERSION}-${DATE}"
      }

      resources {
        # cpu    = 100 # 100 = 100 MHz
        memory = 200 # 128 = 128 MB
        network {
          # mbits = 10
          port "tarantool" {
            static = "3301"  # port used on a host machine
          }
        }
      }

      service {
        name = "tarantool"
        tags = [
          "tarantool",
          "urlprefix-:3301 proto=tcp",
        ]
        port = "tarantool"
        check {
          type     = "tcp"
          interval = "20s"
          timeout  = "2s"
        }
      } # service
    } # task
  } # group






  group "tarantool-master-2" {
    count = 1  # one group of masters
    constraint {
      attribute = "${attr.unique.hostname}"
      value     = "node3"
    }

    restart {
      # mode = "fail"  # fail, delay (default)
      interval = "1m"
      attempts = 1
      delay = "30s"  # minimum wait
    }

    task "tnt-master-2" {
      driver = "docker"
      config {
        image = "registry.gitlab.com/pashinin.com/tarantool:${TAG}"
        force_pull = true
        network_mode = "host"

        port_map {
          tarantool = 3301  # port used inside a container
        }

        # volumes = [
        #   "/var/lib/tarantool:/var/lib/tarantool",
        # ]
        mounts = [
          {
            type = "volume"
            target = "/var/lib/tarantool"
            source = "tarantool-data"
            readonly = false
          },
        ]
      }

      env {
        TARANTOOL_REPLICATION_SOURCE = "10.254.239.1,10.254.239.5"
        VERSION = "master-${VERSION}-${DATE}"
        V = "3"
      }

      resources {
        # cpu    = 100 # 100 = 100 MHz
        memory = 200 # 128 = 128 MB
        network {
          # mbits = 10
          port "tarantool" {
            static = "3301"  # port used on a host machine
          }
        }
      }

      service {
        name = "tarantool"
        tags = [
          "tarantool",
          "urlprefix-:3301 proto=tcp",
        ]
        port = "tarantool"
        check {
          type     = "tcp"
          interval = "20s"
          timeout  = "2s"
        }
      } # service
    } # task
  } # group






  group "tarantool-master-3" {
    count = 1  # one group of 1 master
    constraint {
      attribute = "${attr.unique.hostname}"
      value     = "node5"
    }

    restart {
      # mode = "fail"  # fail, delay (default)
      interval = "1m"
      attempts = 1
      delay = "30s"  # minimum wait
    }

    task "tnt-master-3" {
      driver = "docker"
      config {
        image = "registry.gitlab.com/pashinin.com/tarantool:${TAG}"
        force_pull = true
        network_mode = "host"

        port_map {
          tarantool = 3301  # port used inside a container
        }

        volumes = [
          "/var/lib/tarantool:/var/lib/tarantool",
        ]
        # mounts = [
        #   {
        #     type = "volume"
        #     target = "/var/lib/tarantool"
        #     source = "tarantool-data"
        #     readonly = false
        #   },
        # ]
      }

      env {
        TARANTOOL_REPLICATION_SOURCE = "10.254.239.1,10.254.239.3"
        # TARANTOOL_REPLICATION_SOURCE = "10.254.239.1"
        VERSION = "master-${VERSION}-${DATE}"
      }

      resources {
        # cpu    = 100 # 100 = 100 MHz
        memory = 200 # 128 = 128 MB
        network {
          # mbits = 10
          port "tarantool" {
            static = "3301"  # port used on a host machine
          }
        }
      }

      service {
        name = "tarantool"
        tags = [
          "tarantool",
          "urlprefix-:3301 proto=tcp",
        ]
        port = "tarantool"
        check {
          type     = "tcp"
          interval = "20s"
          timeout  = "2s"
        }
      } # service
    } # task
  } # group
}
