# -*- mode: hcl -*-
job "tarantool-master" {
  datacenters = ["moscow"]
  type = "service"

  constraint {
    attribute = "${meta.instance_type}"
    value = "worker"
  }
  constraint {
    distinct_hosts = true
  }
  constraint {
    attribute = "${attr.unique.hostname}"
    operator  = "!="
    value     = "desktop"
  }

  # Specify this job to have rolling updates, one-at-a-time, with
  # 30 second intervals.
  update {
    stagger      = "30s"
    max_parallel = 1
  }

  group "tarantool" {
    count = 1  # one group of masters

    restart {
      # mode = "fail"  # fail, delay (default)
      interval = "1m"
      attempts = 1
      delay = "30s"  # minimum wait
    }

    network {
      mode = "bridge"
      # port_map {
      # port "tarantool" {
      #   # static = 3301
      #   to = 3301
      # }
    }

    service {
      name = "tarantool-master"
      # port = "tarantool"
      port = "3301"
      connect {
        sidecar_service {
          tags = []
          # proxy {
          #   upstreams {
          #     destination_name = "api-master"
          #     local_bind_port = 9090
          #   }
          # }
        }
      }
      tags = [
        "tarantool",
        "staging",
        # "urlprefix-:3302 proto=tcp",
      ]

      # tcp check are not valid for Connect services
      # check {
      #   type     = "tcp"
      #   port     = "tarantool"
      #   interval = "20s"
      #   timeout  = "2s"
      # }
    } # service

    task "tnt" {
      driver = "docker"
      config {
        image = "registry.gitlab.com/pashinin.com/tarantool:master"
        force_pull = true
        # network_mode = "host"

        # port_map {
        #   tarantool = 3301  # port used inside a container
        # }

        # volumes = [
        #   "/var/lib/tarantool:/var/lib/tarantool",
        # ]
      }

      env {
        VERSION = "master-${VERSION}"
        DATE = "${DATE}"
      }

      resources {
        # cpu    = 100 # 100 = 100 MHz
        memory = 256 # 128 = 128 MB
        network {
          # mbits = 10
          # port "tarantool" {
          #   # static = "3301"  # port used on a host machine
          # }
        }
      }

      # service {
      #   name = "tarantool-master"
      #   tags = [
      #     "tarantool",
      #     "staging",
      #     "urlprefix-:3302 proto=tcp",
      #   ]
      #   port = "tarantool"
      #   check {
      #     type     = "tcp"
      #     interval = "20s"
      #     timeout  = "2s"
      #   }
      # } # service
    } # task
  } # group
}
