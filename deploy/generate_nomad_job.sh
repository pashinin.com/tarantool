#!/bin/sh

mkdir -p tmp
VERSION_TAG=`git rev-parse --short HEAD`
# BRANCH=`git branch | grep \* | cut -d ' ' -f2`

TEMPLATE=nomad-$1.job.tpl

if [ "$CI_COMMIT_SHORT_SHA" == "" ]; then
    export CI_COMMIT_SHORT_SHA="sha1"  # master / stable
fi

TAG="$CI_COMMIT_TAG"
if [ "$TAG" == "" ]; then
    TAG="$CI_COMMIT_REF_NAME"  # master / stable
fi
if [ "$TAG" == "" ] && [ "$1" == "stable" ]; then
    TAG="stable"
fi
if [ "$TAG" == "" ]; then
    TAG="dev"
fi
export TAG=$TAG
export DATE=`date`
# export BRANCH=$BRANCH

# cat nomad-$1.job | docker run -i --rm -e STABLE=stable -e LATEST=master subfuzion/envtpl

# TPL="stable"
# if [ "$CI_COMMIT_REF_NAME" == "master" ]; then
#     TPL="master"
# fi

# subst vars in template:
envsubst < "deploy/$TEMPLATE" | sed -e '/<<EOH.*/rdeploy/nginx.conf'
# '\$DATE \$VERSION \$BRANCH'

# insert nginx.conf content after "<<EOH" string:
# export BRANCH=$BRANCH && sed -e '/<<EOH.*/rnginx.conf' tmp/nomad-$TPL.job-tmp
# > nomad.job
