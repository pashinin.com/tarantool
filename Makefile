IMAGE=tarantool/tarantool:2.5.0
TNTCONTAINER=$(shell docker ps | grep tnt-dev | awk '{print $$1}')

build-docker-image-master:
	docker build --tag registry.gitlab.com/pashinin.com/tarantool:master .

.PHONY: deploy-master
deploy-master:
	sh deploy/generate_nomad_job.sh master | nomad run -

.PHONY: deploy-stable
deploy-stable:
	sh deploy/generate_nomad_job.sh stable | nomad run -

# Run development Tarantool using local binary
dev-local:
	(cd src && exec ~/go/bin/reflex --start-service -r '.*\.lua' -- tarantool app.lua)

# start Tarantool server (dev mode)
dev-docker:
	docker run --name tnt-dev -it --rm -p 3302:3301 \
		-e TARANTOOL_USER_NAME=guest -e TARANTOOL_USER_PASSWORD=password \
		-v `pwd`/src:/opt/tarantool -v dev-tnt:/var/lib/tarantool \
		${IMAGE} tarantool app.lua

# enter console (dev)
# tnt:
# 	docker run --rm -it tarantool/tarantool:2.3.1 tarantoolctl connect localhost:3301
# docker exec -it tnt-dev console

tnt-local:
	docker exec -it ${TNTCONTAINER} console

# enter console (production)
# tarantoolctl connect user:secret@localhost:3301
tnt-prod:
	docker run --rm -it ${IMAGE} tarantoolctl connect 10.254.239.4:3301
