Tarantool
=========

Start development
-----------------

Use one of the following commands. Both commands will start dev server
at :code:`localhost:3302`.

1. Using local :code:`tarantool` binary: :code:`make dev-local`
2. Using Docker image: :code:`make dev-docker`

Hint. Enter dev console with :code:`make tnt`


Add more luarocks modules
-------------------------

1. If Tarantool is installed locally: :code:`tarantoolctl rocks install lua-cjson --server=https://luarocks.org`

2. If using Docker - add to :code:`Dockerfile`:

RUN apk add --no-cache --virtual .build-deps \
    gcc \
    musl-dev \
    wget \
    unzip \
    && tarantoolctl rocks install lua-cjson --server=https://luarocks.org \
    && apk del .build-deps



Working in Tarantool console
----------------------------

:code:`box.space.<TAB>` - list spaces

:code:`box.space.<SPACE>:select{}` - select all records

Note 1. Before switching to a new :code:`TARANTOOL_USER_NAME` - do not forget to
grant permissions to this user:
:code:`box.schema.user.grant('newuser', 'read,write', 'space', 'captchas')`

Note 2. Supported index types:

#. **memtx**: TREE, HASH, RTREE or BITSET
#. **vinyl**: TREE

"captchas" space
----------------

Fields:

#. captcha's ID (string, index - HASH)
#. secret code
#. timestamp when expired

When captcha's timestamp is less than current timestamp - captcha is
removed with :code:`expirationd` module.
